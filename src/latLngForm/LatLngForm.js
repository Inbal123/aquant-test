import React, { useRef, useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Map from '../map/Map';
import styles from './latLngForm.module.css';

const LatLngForm = () => {
    const formRef = useRef(null)
    const [latitude, setLatitude] = useState("")
    const [longtitude, setLongtitude] = useState("")
    const [pushPins, setPushPins] = useState([])

    const handleLatitudeChange = (event) => {
        setLatitude(event.target.value)
    }

    const handleLongtitudeChange = (event) => {
        setLongtitude(event.target.value)
    }

    const handleClick = () => {
        if (formRef.current.reportValidity()) {
            setPushPins([...pushPins, { "location": [parseInt(latitude), parseInt(longtitude)], "option": { color: "red" } }])
            setLatitude("")
            setLongtitude("")
        }
    }

    return (
        <div className={styles.container}>
            <form ref={formRef}>
                <TextField
                    label="Latitude"
                    variant="outlined"
                    type="number"
                    required
                    value={latitude}
                    onChange={handleLatitudeChange}
                    className={styles.textField}
                />
                <TextField
                    label="Longtitude"
                    variant="outlined"
                    type="number"
                    required
                    value={longtitude}
                    onChange={handleLongtitudeChange}
                    className={styles.textField}
                />
                <Button
                    variant="contained"
                    onClick={handleClick}
                    className={styles.button}
                >
                    Add Pushpin
                </Button>
            </form>
            <Map pushPins={pushPins} />
        </div>
    )
}

export default LatLngForm;