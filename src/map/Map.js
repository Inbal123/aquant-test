import React from 'react';
import { ReactBingmaps } from 'react-bingmaps'; 

const Map = ({ pushPins = [] }) => {
    const getPushPinLocations = () => {
        const locations = []

        if (pushPins.length) {
            pushPins.forEach(pushPin => {
                locations.push(pushPin.location)
            })
            
            if (pushPins.length > 1) {
                locations.push(pushPins[0].location)
            }
        }

        return locations
    }

    return (
        <ReactBingmaps
            bingmapKey={process.env.REACT_APP_BING_API_KEY}
            width="100%"
            height="100%"
            center={[32.073582, 34.788052]}
            mapTypeId="road"
            pushPins={[...pushPins]}
            polyline={{
                "location": [...getPushPinLocations()],
                "option": { strokeColor: 'blue', strokeThickness: 5 }
            }}
        >
        </ReactBingmaps>
    )
}

export default Map;