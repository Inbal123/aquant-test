import LatLngForm from "./latLngForm/LatLngForm";

const App = () => {
  return (
    <div>
      <LatLngForm />
    </div>
  )
}

export default App;
